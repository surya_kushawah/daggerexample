package com.wave.dagger;

import javax.inject.Singleton;

import dagger.Component;
/**
 * Created on : Feb 09, 2019
 * Author     : AndroidWave
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(MainActivity target);
}
