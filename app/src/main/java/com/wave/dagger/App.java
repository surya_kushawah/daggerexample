package com.wave.dagger;

import android.app.Application;

/**
 * Created on : Feb 09, 2019
 * Author     : AndroidWave
 */

public class App extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        //needs to run once to generate it
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }


    public AppComponent getComponent() {
        return component;
    }

}
